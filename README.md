# simple-cloud-config

simple-cloud-config is a gradle project meant to illustrate basic usage of a Spring Cloud Config server for managing properties and a client application that consumes them.  Some of what's illustrated:

* Spring Cloud Config server
    * GitHub based repository
    * Config files separated into directories per application
* Spring Boot client
    * Client access to properties via Spring
    * Client access to properties via API
* Gradle multi-project structure

## Usage

Clone the repository and execute the following to start the config server and the client.  The config server reads the properties from the online gitlab repository.

```
./gradlew install
```

Note that the pods might restart a few times until the server is up and running.  Test that the externalized property is returned when you ping the client.

```
YOLO > curl http://localhost:31784
{"propertyViaSpring":"hi there!","propertyViaApi":"hello there!"}
```

## License
[MIT](https://choosealicense.com/licenses/mit/)